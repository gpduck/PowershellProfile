function private:loadmodule {
  param([Parameter(ValueFromPipeline=$true)][string[]]$Name)
  process {
    $Name | ForEach-Object {
      Get-Module -List -Name $_ | Sort-Object Version -Descending | Select -First 1 | Import-Module
    }
  }
}

if($PsVersionTable.PSVersion -ge [version]"3.0.0") {
  @(
    "Microsoft.PowerShell.Management",
    "Microsoft.PowerShell.Utility",
    "Microsoft.PowerShell.Security",
    "cimcmdlets"
  ) | LoadModule
  $PSModuleAutoloadingPreference = "ModuleQualified"
}

if($OutputEncoding.BodyName -ne "utf-8" -or $OutputEncoding.GetPreamble().Length -gt 0) {
  $OutputEncoding = New-Object System.Text.UTF8Encoding($false)
}

if(Get-Module PSReadLine) {
  Remove-PSReadlineKeyHandler Ctrl+Enter
}

function Shruggie {
  param(
    [ValidateSet("Shruggie","TableFlip")]
    $Name = "Shruggie"
  )
  switch($name) {
    "Shruggie" {
      $String = [Text.Encoding]::UTF8.GetString(@(194,175,92,95,40,227,131,132,41,95,47,194,175))
    }
    "TableFlip" {
      $String = [Text.Encoding]::UTF8.GetString(@(40,226,149,175,194,176,226,150,161,194,176,239,188,137,226,149,175,239,184,181,32,226,148,187,226,148,129,226,148,187))
    }
  }
  if($PSVersionTable["Platform"] -eq "Win32NT") {
    [Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") > $null
    [Windows.Forms.clipboard]::Settext($String)
  } else {
    $String
  }
}

$IsAdmin = ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)
if($IsAdmin) {
  function Update-Profile {
    $Script:ProfileUrl = "https://gitlab.com/gpduck/PowershellProfile/raw/master/AllUsersAllHosts.ps1"
    $ProfilePath = $PROFILE.AllUsersAllHosts

    try {
      if(Get-Command -Name Invoke-RestMethod -ErrorAction SilentlyContinue) {
        $ProfileContent = Invoke-RestMethod -Uri $ProfileUrl -Method Get
      } else {
        $Request = [System.Net.WebRequest]::Create($ProfileUrl)
        $Response = $Request.GetResponse()
        if($Response.StatusCode -eq "OK") {
          $Reader = New-Object System.IO.StreamReader($Response.GetResponseStream())
          try {
            $ProfileContent = $Reader.ReadToEnd()
          } finally {
            if($Reader) {
              $Reader.Dispose()
            }
          }
        }
      }
    } catch {
      $ProfileContent = ""
    }
    if(![String]::IsNullOrEmpty($ProfileContent)) {
      Set-Content -Path $ProfilePath -Value $ProfileContent -Confirm:$False
    }
  }
  Start-Job -Name ProfileUpdate -ScriptBlock {
    Update-Profile
  } > $null
}

$Host.PrivateData.VerboseForegroundColor = "green"
@(
  "CredCache",
  "PowerShellGet",
  "Windows",
  "zlocation"
) | LoadModule

function HowLongWasThat {
  param(
    $HistoryId
  )
  if($HistoryId) {
    $History = Get-History -Id $HistoryId
  } else {
    $History = Get-History | Select -Last 1
  }
  if($History) {
    $History.EndExecutionTime - $History.StartExecutionTime
  }
}

function gvsv {
  get-verb | sort verb
}

function touch {
    param($Path)
    set-content -path $Path -Value $Null -Encoding Byte
}

function Export {
  param(
    $EnvironmentVariable
  )
  $Name,$Value = $EnvironmentVariable.Split("=")
  [Environment]::SetEnvironmentVariable($Name, $Value, "Process")
}

if(Get-Module "PowerShellGet") {
  function Find-ModuleUpdates {
    param(
      [string[]]$Repository
    )


    $Repos = @{}
    if($Repository) {
      Get-PSRepository -Name $Repository | ForEach-Object {
        $Repos[$_.SourceLocation] = $_
      }
    } else {
      Get-PSRepository | Where-Object {$_.InstallationPolicy -eq "Trusted"} | ForEach-Object {
        $Repos[$_.SourceLocation] = $_
      }
    }

    Get-Module -List | Where-Object { $_.RepositorySourceLocation -and $_.RepositorySourceLocation.ToString() -in $Repos.Keys } | Group-Object Name | ForEach-Object {
      $LocalModule = $_.Group | Sort-Object -Property Version -Descending | Select-Object -First 1
      $RepoModule = Find-Module -Name $LocalModule.Name -Repository $Repos[$LocalModule.RepositorySourceLocation.ToString()].Name
      if($RepoModule.Version -gt $LocalModule.Version) {
        $RepoModule
      }
    }
  }

  function Remove-OldModuleVersions {
    [CmdletBinding(SupportsShouldProcess=$true)]
    param(
      [ValidateSet("CurrentUser","AllUsers")]
      [String]$Scope
    )

    Get-Module -ListAvailable | Where-Object {
      $ModuleInfo = $_
      switch($Scope) {
        "AllUsers" {
          $ModuleInfo.Path.StartsWith("${env:ProgramFiles}\WindowsPowerShell\Modules")
        }
        "CurrentUser" {
          $moduleInfo.Path.StartsWith("${env:UserProfile}\Documents\WindowsPowerShell\Modules")
        }
        default {
          $true
        }
      }
    } |  Group-Object Name | ForEach-Object {
      $OldVersions = $_.Group | Sort-Object -Property Version -Descending | Select-Object -Skip 1
      if($OldVersions) {
        $OldVersions | ForEach-Object {
          if($PSCmdlet.ShouldProcess("$($_.Name) v$($_.Version)", "Remove module")) {
            Uninstall-Module -RequiredVersion $_.Version -Name $_.Name -Force
          }
        }
      }
    }
  }
}

if($Host.Ui.RawUI.WindowTitle -and !$Host.Ui.RawUI.WindowTitle.EndsWith("($PID)")) {
  $Host.UI.RawUI.WindowTitle += " ($PID)"
}

function global:prompt {
    $realLASTEXITCODE = $LASTEXITCODE

    Write-Host "PS $($executionContext.SessionState.Path.CurrentLocation)" -NoNewLine
    if((Test-Path variable:VcsPromptStatuses) -and $global:VcsPromptStatuses) {
        Write-VcsStatus
    }
    return "$('>' * ($nestedPromptLevel + 1)) "

    $global:LASTEXITCODE = $realLASTEXITCODE
}

function Add-OpenWithCode {
  [CmdletBinding(SupportsShouldProcess=$true)]
  param(
    [ValidateSet("Files","Folders")]
    [String[]]$Type = @("Files","Folders")
  )
  $RegValueName = "VSCode"
  $ShellNameShort = "Code"
  $App = "C:\Program Files\Microsoft VS Code"
  $ExeBasename = "Code"

  function SetRegistryValue {
    [CmdletBinding(SupportsShouldProcess=$true)]
    param(
      [Microsoft.Win32.RegistryHive]$Hive,
      $Path,
      $Name,
      $Value,
      [Microsoft.Win32.RegistryValueKind]$Kind = "String"
    )
    $Base = [Microsoft.Win32.RegistryKey]::OpenBaseKey($Hive, [Microsoft.Win32.RegistryView]::Default)
    $Key = $Base.OpenSubKey($Path, $true)
    if(-not $Key -and $PSCmdlet.ShouldProcess("$($Base.Name)\$Path", "Create registry key")) {
      $Key = $Base.CreateSubKey($Path)
      Write-Debug "Created key"
    }
    if(-not $Key -or $Value -ne $Key.GetValue($Name)) {
      if($PSCmdlet.ShouldProcess("$($Base.Name)\$Path\$Name", "Set registry value")) {
        $Key.SetValue($Name, $Value, $Kind)
        Write-Debug "Set value"
      }
    }
  }

  switch ($Type) {
    "Files" {
      SetRegistryValue -Hive "CurrentUser" -Path "Software\Classes\*\shell\$RegValueName" -Name "" -Value "Open w&ith $ShellNameShort"
      SetRegistryValue -Hive "CurrentUser" -Path "Software\Classes\*\shell\$RegValueName" -Name "Icon" -Value "${App}\${ExeBaseName}.exe"
      SetRegistryValue -Hive "CurrentUser" -Path "Software\Classes\*\shell\$RegValueName\command" -Name "" -Value "`"${App}\${ExeBaseName}.exe`" `"%1`""
    }
    "Folders" {
      SetRegistryValue -Hive "CurrentUser" -Path "Software\Classes\directory\shell\$RegValueName" -Name "" -Value "Open w&ith $ShellNameShort"
      SetRegistryValue -Hive "CurrentUser" -Path "Software\Classes\directory\shell\$RegValueName" -Name "Icon" -Value "${App}\${ExeBaseName}.exe"
      SetRegistryValue -Hive "CurrentUser" -Path "Software\Classes\directory\shell\$RegValueName\command" -Name "" -Value "`"${App}\${ExeBaseName}.exe`" `"%V`""

      SetRegistryValue -Hive "CurrentUser" -Path "Software\Classes\directory\background\shell\$RegValueName" -Name "" -Value "Open w&ith $ShellNameShort"
      SetRegistryValue -Hive "CurrentUser" -Path "Software\Classes\directory\background\shell\$RegValueName" -Name "Icon" -Value "${App}\${ExeBaseName}.exe"
      SetRegistryValue -Hive "CurrentUser" -Path "Software\Classes\directory\background\shell\$RegValueName\command" -Name "" -Value "`"${App}\${ExeBaseName}.exe`" `"%V`""

      SetRegistryValue -Hive "CurrentUser" -Path "Software\Classes\Drive\shell\$RegValueName" -Name "" -Value "Open w&ith $ShellNameShort"
      SetRegistryValue -Hive "CurrentUser" -Path "Software\Classes\Drive\shell\$RegValueName" -Name "Icon" -Value "${App}\${ExeBaseName}.exe"
      SetRegistryValue -Hive "CurrentUser" -Path "Software\Classes\Drive\shell\$RegValueName\command" -Name "" -Value "`"${App}\${ExeBaseName}.exe`" `"%V`""
    }
  }
}

$LocalProfile = Join-Path (Split-Path $PROFILE.AllUsersAllHosts -Parent) "profile.local.ps1"
if(Test-Path $LocalProfile) {
  & $LocalProfile
}

Remove-Item function:\loadmodule -ErrorAction SilentlyContinue
Remove-Item variable:\IsAdmin -ErrorAction SilentlyContinue
Remove-Item variable:\ProfilePath -ErrorAction SilentlyContinue
Remove-Item variable:\LocalProfile -ErrorAction SilentlyContinue